ghcid-devel: ## Run the server in fast development mode. See DevelMain for details.
	ghcid \
	    --command "stack ghci RealWorldTM" \
	    --test "DevelMain.update" \

.PHONY: ghcid-devel
