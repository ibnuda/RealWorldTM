{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeOperators         #-}
module API.User where

import           Protolude

import           Servant
import           Servant.Auth.Server

import           Import

import           Handler.UserInformation

type UserInformationAPI =
  "user"
    :> Get '[ JSON] ResponseUser
  :<|> "user"
    :> ReqBody '[ JSON] RequestUpdateUser
    :> Put '[ JSON] ResponseUser

userInformationApi ::
     MonadIO m => AuthResult User -> ServerT UserInformationAPI (AppT m)
userInformationApi authres =
  getUserInformationHandler authres
  :<|> putUserInformationHandler authres

userInformationProxy :: Proxy UserInformationAPI
userInformationProxy = Proxy

userInformationServer ::
     Configuration -> AuthResult User -> Server UserInformationAPI
userInformationServer conf authres =
  hoistServer
    userInformationProxy
    (convertApp conf)
    (userInformationApi authres)

type UserAdministrationAPI =
  "users"
    :> ReqBody '[ JSON] RequestRegistration
    :> Post '[ JSON] ResponseUser
  :<|> "users"
    :> "login"
    :> ReqBody '[JSON] RequestLogin
    :> Post '[ JSON] ResponseUser

userAdministrationApi :: MonadIO m => ServerT UserAdministrationAPI (AppT m)
userAdministrationApi =
  postRegistrationHandler
  :<|> postLoginHandler

userAdministrationProxy :: Proxy UserAdministrationAPI
userAdministrationProxy = Proxy

userAdministrationServer :: Configuration -> Server UserAdministrationAPI
userAdministrationServer conf =
  hoistServer userAdministrationProxy (convertApp conf) userAdministrationApi
