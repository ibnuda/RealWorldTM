{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}
module API.Articles where

import           Protolude

import           Servant
import           Servant.Auth.Server

import           Import

import           Handler.Articles

type ArticleAPI =
  "articles"
    :> QueryParam "tag" Text
    :> QueryParam "author" Text
    :> QueryParam "favorited" Text
    :> QueryParam "limit" Int64
    :> QueryParam "offset" Int64
    :> Get '[ JSON] ResponseMultiArticle
  :<|> "articles"
    :> "feed"
    :> QueryParam "limit" Int64
    :> QueryParam "offset" Int64
    :> Get '[ JSON] ResponseMultiArticle
  :<|> "articles"
    :> Capture "slug" Text
    :> Get '[ JSON] ResponseArticle
  :<|> "articles"
    :> ReqBody '[ JSON] RequestCreateArticle
    :> Post '[ JSON] ResponseArticle
  :<|> "articles"
    :> Capture "slug" Text
    :> DeleteNoContent '[ JSON] NoContent
  :<|> "articles"
    :> Capture "slug" Text
    :> ReqBody '[ JSON] RequestUpdateArticle
    :> Put '[ JSON] ResponseArticle
  :<|> "articles"
    :> Capture "slug" Text
    :> "comments"
    :> Get '[JSON] ResponseMultiComment
  :<|> "articles"
    :> Capture "slug" Text
    :> "comments"
    :> ReqBody '[ JSON] RequestComment
    :> Post '[JSON] ResponseComment
  :<|> "articles"
    :> Capture "slug" Text
    :> "comments"
    :> Capture "id" Int64
    :> DeleteNoContent '[ JSON] NoContent
  :<|> "articles"
    :> Capture "slug" Text
    :> "favorite"
    :> PostNoContent '[ JSON] NoContent
  :<|> "articles"
    :> Capture "slug" Text
    :> "favorite"
    :> DeleteNoContent '[ JSON] NoContent

articleSlugProxy :: Proxy ArticleAPI
articleSlugProxy = Proxy

articleSlugApi ::
     MonadIO m => AuthResult User -> ServerT ArticleAPI (AppT m)
articleSlugApi authres =
  getArticlesHandler authres
  :<|> getFeedHandler authres
  :<|> getArticleSlugHandler authres
  :<|> postArticleHandler authres
  :<|> deleteArticleHandler authres
  :<|> putArticleHandler authres
  :<|> getCommentsArticleHandler authres
  :<|> postCommentToArticleHandler authres
  :<|> deleteCommentHandler authres
  :<|> postFavoriteArticleHandler authres
  :<|> deleteFavoriteArticleHandler authres

articleSlugServer :: Configuration -> AuthResult User -> Server ArticleAPI
articleSlugServer conf authres =
  hoistServer articleSlugProxy (convertApp conf) (articleSlugApi authres)
