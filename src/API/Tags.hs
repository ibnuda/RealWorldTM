{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeOperators         #-}
module API.Tags where

import           Protolude

import           Servant

import           Import

import           Handler.Tags

type TagsAPI = "tags" :> Get '[ JSON] ResponseTags

tagsApi :: MonadIO m => ServerT TagsAPI (AppT m)
tagsApi = getTagsHandler

tagsProxy :: Proxy TagsAPI
tagsProxy = Proxy

tagsServer :: Configuration -> Server TagsAPI
tagsServer conf = hoistServer tagsProxy (convertApp conf) tagsApi
