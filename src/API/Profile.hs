{-# LANGUAGE DataKinds        #-}
{-# LANGUAGE DeriveGeneric    #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TemplateHaskell  #-}
{-# LANGUAGE TypeOperators    #-}
module API.Profile where

import           Protolude

import           Servant
import           Servant.Auth.Server

import           Import

import           Handler.UserProfile

type UserProfileAPI =
  "profiles"
    :> Capture "username" Text
    :> Get '[ JSON] ResponseProfile
  :<|> "profiles"
    :> Capture "username" Text
    :> "follow"
    :> PostNoContent '[ JSON] NoContent
  :<|> "profiles"
    :> Capture "username" Text
    :> "follow"
    :> DeleteNoContent '[ JSON] NoContent

userProfileApi :: MonadIO m => AuthResult User -> ServerT UserProfileAPI (AppT m)
userProfileApi authres =
  getUserProfileHandler authres
  :<|> postUserProfileFollowHandler authres
  :<|> deleteUserProfileFollowHandler authres

userProfileProxy :: Proxy UserProfileAPI
userProfileProxy = Proxy

userProfileServer :: Configuration -> AuthResult User -> Server UserProfileAPI
userProfileServer conf authres =
  hoistServer userProfileProxy (convertApp conf) (userProfileApi authres)
