{-# LANGUAGE DeriveFunctor              #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Conf where

import           Protolude

import           Database.Persist.Postgresql
import           Servant
import           Servant.Auth.Server

data Configuration = Configuration
  { configurationPool        :: ConnectionPool
  , configurationJWTSettings :: JWTSettings
  }

newtype AppT m a = AppT
  { runApp :: ReaderT Configuration (ExceptT ServantErr m) a
  } deriving ( Functor
             , Applicative
             , Monad
             , MonadReader Configuration
             , MonadError ServantErr
             , MonadIO
             )

type App = AppT IO

convertApp :: Configuration -> AppT IO a -> Handler a
convertApp conf appt = Handler $ runReaderT (runApp appt) conf
