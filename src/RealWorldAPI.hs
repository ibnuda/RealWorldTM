{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE DeriveGeneric   #-}
{-# LANGUAGE KindSignatures  #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeOperators   #-}
module RealWorldAPI where

import           Protolude

import           Control.Monad.Logger
import           Database.Persist.Postgresql
import           Network.Wai.Handler.Warp
import           Network.Wai.Handler.Warp            (Port)
import           Servant
import           Servant.Auth
import           Servant.Auth.Server
import           Servant.Auth.Server.SetCookieOrphan ()

import           Import

import           API.User
import           API.Profile
import           API.Articles
import           API.Tags

type RealWorldAPI auth
   = "api" :> (Servant.Auth.Server.Auth auth User :> UserInformationAPI)
   :<|> "api" :> UserAdministrationAPI
   :<|> "api" :> (Servant.Auth.Server.Auth auth User :> UserProfileAPI)
   :<|> "api" :> (Servant.Auth.Server.Auth auth User :> ArticleAPI)
   :<|> "api" :> TagsAPI
   :<|> Raw

realWorldProxy :: Proxy (RealWorldAPI '[JWT])
realWorldProxy = Proxy

realWorldServer :: Configuration -> Server (RealWorldAPI auth)
realWorldServer conf =
  userInformationServer conf
  :<|> userAdministrationServer conf
  :<|> userProfileServer conf
  :<|> articleSlugServer conf
  :<|> tagsServer conf
  :<|> serveDirectoryFileServer "front"

connstring :: ByteString
connstring =
  "host=localhost "
  <> "port=5432 "
  <> "user=ibnu "
  <> "password=jaran "
  <> "dbname=uwu"

running :: IO ()
running = do
  jwk <- generateKey
  pool <- runStderrLoggingT $ createPostgresqlPool connstring 10
  let jws = defaultJWTSettings jwk
      cfg = defaultCookieSettings :. jws :. EmptyContext
      conf = Configuration pool jws
  runSqlPool doMigration pool
  run 8080 (serveWithContext realWorldProxy cfg (realWorldServer conf))

stop :: IO ()
stop = return ()

startServe :: IO (Port, Application)
startServe = do
  jwk <- generateKey
  pool <- runStderrLoggingT $ createPostgresqlPool connstring 10
  let jws = defaultJWTSettings jwk
      cfg = defaultCookieSettings :. jws :. EmptyContext
      conf = Configuration pool jws
  runSqlPool doMigration pool
  return (8080, serveWithContext realWorldProxy cfg (realWorldServer conf))
