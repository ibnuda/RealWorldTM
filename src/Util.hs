{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
module Util where

import           Protolude

import           Crypto.BCrypt
import qualified Data.ByteString.Char8 as C
import qualified Data.ByteString.Lazy  as BL
import           Data.Text             as T
import           Servant
import           Servant.Auth.Server
import           Text.Regex            (mkRegex, subRegex)

import           Conf
import           Model
import           Types

userToResponseUser :: User -> Maybe Text -> ResponseUserBody
userToResponseUser User {..} token =
  ResponseUserBody
  { respuserbodyEmail = userEmail
  , respuserbodyToken = token
  , respuserbodyUsername = userUsername
  , respuserbodyImage = userImage
  , respuserbodyBio = userBio
  }

userToResponseProfile :: User -> Bool -> ResponseProfileBody
userToResponseProfile User {..} following =
  ResponseProfileBody
  { respprofbodyUsername = userUsername
  , respprofbodyBio = userBio
  , respprofbodyImage = userImage
  , respprofbodyFollowing = following
  }

getJWS :: (MonadReader Configuration m) => m JWTSettings
getJWS = do
  (asks configurationJWTSettings)

eitherToHandler :: MonadError e m => Either a1 t -> (t -> a2) -> e -> m a2
eitherToHandler (Left _) _ onFail     = throwError onFail
eitherToHandler (Right v) onSuccess _ = return $ onSuccess v

maybeToHandler ::
     (MonadIO m, MonadError ServantErr m)
  => (t -> a)
  -> ServantErr
  -> Maybe t
  -> m a
maybeToHandler _ onFail  Nothing    = throwError onFail
maybeToHandler onSuccess _ (Just v) = return $ onSuccess v

maybeNotAuthorized :: MonadError ServantErr m => Maybe a -> m a
maybeNotAuthorized Nothing  = throwError err401
maybeNotAuthorized (Just x) = return x

trueToHandler :: MonadError ServantErr m => Bool -> m ()
trueToHandler True  = return ()
trueToHandler False = throwError err401

textToBChar :: T.Text -> C.ByteString
textToBChar = C.pack . T.unpack

isAllNothing :: RequestUpdateUserBody -> Bool
isAllNothing RequestUpdateUserBody {..} =
  isNothing requpdtuserbodyBio
  && isNothing requpdtuserbodyEmail
  && isNothing requpdtuserbodyImage
  && isNothing requpdtuserbodyPassword
  && isNothing requpdtuserbodyUsername

generatePassword :: Text -> IO Text
generatePassword password = do
  mpass <- hashPasswordUsingPolicy slowerBcryptHashingPolicy (textToBChar password)
  case mpass of
    Nothing -> generatePassword password
    Just x  -> return (decodeUtf8 x)

generateToken :: MonadIO m => User -> AppT m Text
generateToken user = do
  jws <- getJWS
  etoken <- liftIO (makeJWT user jws Nothing)
  token <- eitherToHandler etoken (decodeUtf8 . BL.toStrict) err500
  return token

articleToResponse ::
     Article
  -> Maybe [Text] -- ^ Tags.
  -> Bool -- ^ Favorited.
  -> Int64 -- ^ Favorited counts.
  -> User -- ^ Author.
  -> Bool -- ^ Following?
  -> ResponseArticleBody
articleToResponse Article {..} tags favorited favoritedcounts author following =
  (ResponseArticleBody
     articleSlug
     articleTitle
     articleDescription
     articleBody
     tags
     articleCreatedAt
     articleUpdatedAt
     favorited
     favoritedcounts
     (userToResponseProfile author following))

authresToMaybe :: AuthResult a -> Maybe a
authresToMaybe (Authenticated x) = Just x
authresToMaybe _                 = Nothing

generateSlug :: Text -> Text -> Text
generateSlug title descr = (smaller title) <> "-" <> (smaller descr)
  where
    smaller sentence =
      toLower (pack (subRegex (mkRegex "[^a-zA-Z0-9_.]") (unpack sentence) "-"))

errorDoesnotExists :: MonadIO m => [a] -> AppT m ()
errorDoesnotExists [] = return ()
errorDoesnotExists _  = throwError err404

errorAlreadyExists :: MonadIO m => [a] -> AppT m ()
errorAlreadyExists [] = return ()
errorAlreadyExists _  = throwError err409
