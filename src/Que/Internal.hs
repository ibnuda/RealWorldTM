{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE Rank2Types            #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE TypeFamilies          #-}
module Que.Internal where

import           Protolude                            hiding (from, get, on,
                                                       (<&>))

import           Database.Esqueleto
import           Database.Esqueleto.Internal.Language

import           Model

checkExistenceOfTwoFields ::
     ( PersistField typ4
     , PersistField typ3
     , PersistField typ2
     , PersistField typ1
     , PersistUniqueRead backend
     , PersistQueryRead backend
     , BackendCompatible SqlBackend backend
     , BackendCompatible SqlBackend (PersistEntityBackend val3)
     , BackendCompatible SqlBackend (PersistEntityBackend val2)
     , BackendCompatible SqlBackend (PersistEntityBackend val1)
     , PersistEntity val3
     , PersistEntity val2
     , PersistEntity val1
     , MonadIO m
     )
  => typ2
  -> EntityField val1 typ2
  -> typ4
  -> EntityField val3 typ4
  -> EntityField val1 typ3
  -> EntityField val2 typ3
  -> EntityField val2 typ1
  -> EntityField val3 typ1
  -> ReaderT backend m [Value typ2]
checkExistenceOfTwoFields fleft flacc fright fracc accleft accmidleft accmidright accright = do
  select $
    from $ \(left `InnerJoin` middle `InnerJoin` right) -> do
      on (middle ^. accmidright ==. right ^. accright)
      on (left ^. accleft ==. middle ^. accmidleft)
      where_ (left ^. flacc ==. val fleft)
      where_ (right ^. fracc ==. val fright)
      return (left ^. flacc)

checkExistenceOfFollowing ::
     ( MonadIO m
     , BackendCompatible SqlBackend backend
     , PersistQueryRead backend
     , PersistUniqueRead backend
     )
  => Text
  -> Text
  -> ReaderT backend m [Value Text]
checkExistenceOfFollowing follower author = do
  checkExistenceOfTwoFields
    follower
    UserUsername
    author
    UserUsername
    UserId
    FollowFollowerId
    FollowAuthorId
    UserId

checkExistenceOfFavorite ::
     ( MonadIO m
     , BackendCompatible SqlBackend backend
     , PersistQueryRead backend
     , PersistUniqueRead backend
     )
  => Text
  -> Text
  -> ReaderT backend m [Value Text]
checkExistenceOfFavorite username slug = do
  checkExistenceOfTwoFields
    username
    UserUsername
    slug
    ArticleSlug
    UserId
    FavoritedUserId
    FavoritedArticleId
    ArticleId

updateMaybeByMaybe ::
     (PersistField a, Esqueleto query expr backend, PersistEntity val)
  => Maybe a
  -> EntityField val (Maybe a)
  -> expr (Update val)
updateMaybeByMaybe (Just x) accessor = accessor =. val (Just x)
updateMaybeByMaybe Nothing  accessor = accessor =. val Nothing

updateByMaybe ::
     (PersistField typ, Esqueleto query expr backend, PersistEntity val)
  => Maybe typ
  -> expr (Entity val)
  -> EntityField val typ
  -> expr (Update val)
updateByMaybe (Just x) _     accessor = accessor =. val x
updateByMaybe Nothing entity accessor = accessor =. entity ^. accessor

whereDepends ::
     ( PersistField typ
     , PersistEntity val2
     , PersistEntity val1
     , Esqueleto query expr backend
     )
  => Maybe a
  -> expr (Entity val1)
  -> EntityField val1 typ
  -> expr (Entity val2)
  -> EntityField val2 typ
  -> query ()
whereDepends (Just _) enta accessa entb accessb =
  where_ (enta ^. accessa ==. entb ^. accessb)
whereDepends Nothing _ _ _ _ = return ()

whereQuery ::
     (PersistField typ, Esqueleto query expr backend, PersistEntity val)
  => Maybe typ
  -> expr (Entity val)
  -> EntityField val typ
  -> query ()
whereQuery Nothing _ _              = return ()
whereQuery (Just x) entity accessor = where_ (entity ^. accessor ==. val x)
