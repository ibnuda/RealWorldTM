{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE Rank2Types            #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE TypeFamilies          #-}
module Que.User where

import           Protolude          hiding (from, get, on, (<&>))

import           Database.Esqueleto

import           Model
import           Types

import           Que.Internal

getUserByUsername ::
     (BaseBackend backend ~ SqlBackend, PersistUniqueRead backend, MonadIO m)
  => Text
  -> ReaderT backend m (Maybe (Entity User))
getUserByUsername username = getBy (UniqueUsername username)

selectUserByEmail ::
     (BaseBackend backend ~ SqlBackend, PersistUniqueRead backend, MonadIO m)
  => Text
  -> ReaderT backend m (Maybe (Entity User))
selectUserByEmail email = getBy (UniqueEmail email)

selectUserProfileByUsername ::
     ( MonadIO m
     , BackendCompatible SqlBackend backend
     , PersistQueryRead backend
     , PersistUniqueRead backend
     )
  => Maybe Text
  -> Text
  -> ReaderT backend m [(Entity User, Value Bool)]
selectUserProfileByUsername (Just username) authorname = do
  select $
    from $ \author -> do
      let isfollowing =
            case_
              [ when_
                  (exists $
                   from $ \(follow, follower) -> do
                     where_ (follow ^. FollowAuthorId ==. author ^. UserId)
                     where_ (follow ^. FollowFollowerId ==. follower ^. UserId)
                     where_ (follower ^. UserUsername ==. val username))
                  then_
                  (val True)
              ]
              (else_ (val False))
      where_ (author ^. UserUsername ==. val authorname)
      return (author, isfollowing)
selectUserProfileByUsername Nothing authorname = do
  select $
    from $ \author -> do
      where_ (author ^. UserUsername ==. val authorname)
      limit 1
      return (author, val False)

insertUserFromRegistration ::
     (BaseBackend backend ~ SqlBackend, PersistStoreWrite backend, MonadIO m)
  => RequestRegistrationBody
  -> ReaderT backend m (Maybe User)
insertUserFromRegistration RequestRegistrationBody {..} = do
  id <-
    insert $
    User reqregbodyEmail reqregbodyUsername reqregbodyPassword Nothing Nothing
  get id

insertFollowing ::
     ( PersistUniqueWrite backend
     , PersistQueryWrite backend
     , BackendCompatible SqlBackend backend
     , MonadIO m
     )
  => Text
  -> Text
  -> ReaderT backend m ()
insertFollowing followername authorname = do
  insertSelect $
    from $ \(author, follower) -> do
      where_ (follower ^. UserUsername ==. val followername)
      where_ (author ^. UserUsername ==. val authorname)
      return $ Follow <# (follower ^. UserId) <&> (author ^. UserId)

deleteFollowing ::
     ( PersistUniqueWrite backend
     , PersistQueryWrite backend
     , BackendCompatible SqlBackend backend
     , MonadIO m
     )
  => Text
  -> Text
  -> ReaderT backend m ()
deleteFollowing followername authorname = do
  delete $
    from $ \following -> do
      where_ $
        exists $
        from $ \(follower, author) -> do
          where_ (following ^. FollowFollowerId ==. follower ^. UserId)
          where_ (following ^. FollowAuthorId ==. author ^. UserId)
          where_ (follower ^. UserUsername ==. val followername)
          where_ (author ^. UserUsername ==. val authorname)

updateUser ::
     MonadIO m
  => Text
  -> Maybe Text
  -> Maybe Text
  -> Maybe Text
  -> Maybe Text
  -> Maybe Text
  -> ReaderT SqlBackend m ()
updateUser oldname email username password bio image = do
  update $ \user -> do
    set
      user
      [ updateByMaybe email user UserEmail
      , updateByMaybe username user UserUsername
      , updateByMaybe password user UserPassword
      , updateMaybeByMaybe bio UserBio
      , updateMaybeByMaybe image UserImage
      ]
    where_ (user ^. UserUsername ==. val oldname)

isNarcissist ::
     ( PersistUniqueRead backend
     , PersistQueryRead backend
     , BackendCompatible SqlBackend backend
     , MonadIO m
     )
  => Text
  -> ReaderT backend m [Entity User]
isNarcissist username = do
  select $ from $ \user -> do
    where_ $ notExists $
      from $ \(following) -> do
        where_ (following ^. FollowFollowerId ==. user ^. UserId)
        where_ (following ^. FollowAuthorId ==. user ^. UserId)
        where_ (user ^. UserUsername ==. val username)
    limit 1
    return user
