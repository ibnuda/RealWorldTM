{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE Rank2Types            #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE TypeFamilies          #-}
module Que.Articles where

import           Protolude                     hiding (from, get, on, (<&>))

import           Data.Time
import           Database.Esqueleto
import           Database.Esqueleto.Internal.Language
import           Database.Esqueleto.PostgreSQL

import           Model
import           Types
import           Util

import           Que.Internal
import           Que.User

insertArticle ::
     ( BaseBackend backend ~ SqlBackend
     , PersistUniqueWrite backend
     , PersistQueryRead backend
     , BackendCompatible SqlBackend backend
     , MonadIO m
     )
  => Text
  -> RequestCreateArticleBody
  -> ReaderT backend m ResponseArticle
insertArticle username RequestCreateArticleBody {..} = do
  (Just (Entity uid user)) <- getUserByUsername username
  now <- liftIO getCurrentTime
  let favorited = False
      favcounts = 0
      article =
        Article
          (generateSlug reqcrtarticlTitle reqcrtarticlDescription)
          uid
          reqcrtarticlTitle
          reqcrtarticlDescription
          reqcrtarticlBody
          now
          Nothing
  martid <- insertUnique article
  menttags <- upsertTags reqcrtarticlTagList
  insertTagged martid menttags
  return
    (ResponseArticle
       (articleToResponse
          article
          reqcrtarticlTagList
          favorited
          favcounts
          user
          False))

upsertTags ::
     (BaseBackend backend ~ SqlBackend, PersistUniqueWrite backend, MonadIO m)
  => Maybe [Text]
  -> ReaderT backend m [Entity Tag]
upsertTags (Just xs) = mapM (flip upsert []) (map Tag xs)
upsertTags Nothing   = return []

insertTagged ::
     (BaseBackend backend ~ SqlBackend, PersistUniqueWrite backend, MonadIO m)
  => Maybe (Key Article)
  -> [Entity Tag]
  -> ReaderT backend m ()
insertTagged (Just artid) tags = mapM_ (flip upsert []) (map (Tagged artid) (map entityKey tags))
insertTagged _ _               = return ()

selectLatestArticle ::
     ( PersistUniqueRead backend
     , PersistQueryRead backend
     , BackendCompatible SqlBackend backend
     , MonadIO m
     )
  => Maybe Text -- ^ Request's username.
  -> Bool -- ^ Is feed?
  -> Maybe Text -- ^ Slug name.
  -> Maybe Text -- ^ Tag name.
  -> Maybe Text -- ^ Author's name.
  -> Maybe Text -- ^ Favorited by.
  -> Int64 -- ^ Limit.
  -> Int64 -- ^ Offset.
  -> ReaderT backend m [( Entity Article
                        , Entity User
                        , Value (Maybe [Text])
                        , Value Int
                        , Value Bool
                        , Value Bool)]
selectLatestArticle musername isfeed mslug mtag maut mfav lim off = do
  select $
    from $ \(art `InnerJoin` aut `LeftOuterJoin` tagged `LeftOuterJoin` tag `LeftOuterJoin` favorited `LeftOuterJoin` favor) -> do
      on (favorited ^. FavoritedUserId ==. favor ^. UserId)
      on (favorited ^. FavoritedArticleId ==. art ^. ArticleId)
      on (tag ^. TagId ==. tagged ^. TaggedTagId)
      on (art ^. ArticleId ==. tagged ^. TaggedArticleId)
      on (aut ^. UserId ==. art ^. ArticleAuthorId)
      let following Nothing = val False
          following (Just username) =
            case_
              [ when_
                  (exists $
                   from $ \(follow', user') -> do
                     where_ (follow' ^. FollowAuthorId ==. aut ^. UserId)
                     where_ (follow' ^. FollowFollowerId ==. user' ^. UserId)
                     where_ (user' ^. UserUsername ==. val username))
                  then_
                  (val True)
              ]
              (else_ (val False))
          favoriting Nothing = val False
          favoriting (Just username) =
            case_
              [ when_
                  (exists $
                   from $ \(favorited', user') -> do
                     where_
                       (favorited' ^. FavoritedArticleId ==. art ^. ArticleId)
                     where_ (favorited' ^. FavoritedUserId ==. user' ^. UserId)
                     where_ (user' ^. UserUsername ==. val username))
                  then_
                  (val True)
              ]
              (else_ (val False))
          favors =
            sub_select $
            from $ \(user', favorited') -> do
              where_ (user' ^. UserId ==. favorited' ^. FavoritedUserId)
              where_ (favorited' ^. FavoritedArticleId ==. art ^. ArticleId)
              return (count (user' ^. UserId))
          tags =
            sub_select $
            from $ \(tag', tagged') -> do
              where_ (tag' ^. TagId ==. tagged' ^. TaggedTagId)
              where_ (tagged' ^. TaggedArticleId ==. art ^. ArticleId)
              return (arrayAgg (tag' ^. TagName))
      whereQuery mtag tag TagName
      whereQuery maut aut UserUsername
      whereQuery mfav favor UserUsername
      whereQuery mslug art ArticleSlug
      whereSublist
        isfeed
        musername
        aut
        UserId
        (feed UserId FollowFollowerId FollowAuthorId UserId UserUsername)
      groupBy (art ^. ArticleId)
      groupBy (aut ^. UserId)
      limit lim
      offset off
      return
        ( art
        , aut
        -- uncomment this line to get a runtime exception.
        -- , arrayAgg (tag ^. TagName)
        , tags
        , favors
        , favoriting musername
        , following musername)

feed ::
     ( FromPreprocess query expr backend (expr (Entity User))
     , FromPreprocess query expr backend (expr (Entity Follow))
     , FromPreprocess query expr backend (expr (Entity User))
     )
  => EntityField User (Key User)
  -> EntityField Follow (Key User)
  -> EntityField Follow (Key User)
  -> EntityField User (Key User)
  -> EntityField User Text
  -> Text
  -> query (expr (Value (Key User)))
feed acclef accmidlef accmidrig accrig accinp uname = do
  from $ \(elef `InnerJoin` emid `InnerJoin` erig) -> do
    on (emid ^. accmidrig ==. erig ^. accrig)
    on (elef ^. acclef ==. emid ^. accmidlef)
    where_ (elef ^. accinp ==. val uname)
    return (erig ^. accrig )

whereSublist ::
     Esqueleto query expr backend
  => Bool
  -> Maybe Text
  -> expr (Entity User)
  -> EntityField User (Key User)
  -> (Text -> query (expr (Value (Key User))))
  -> query ()
whereSublist True (Just uname) ent acc f = where_ (ent ^. acc `in_` (subList_select (f uname)))
whereSublist True Nothing _ _ _ = return ()
whereSublist False _ _ _ _    = return ()


deleteArticleBySlug ::
     ( PersistUniqueWrite backend
     , PersistQueryWrite backend
     , BackendCompatible SqlBackend backend
     , MonadIO m
     )
  => Text
  -> Text
  -> ReaderT backend m ()
deleteArticleBySlug username slugname = do
  delete $
    from $ \article -> do
      where_ $
        exists $
        from $ \author -> do
          where_ (author ^. UserUsername ==. val username)
          where_ (author ^. UserId ==. article ^. ArticleAuthorId)
          where_ (article ^. ArticleSlug ==. val slugname)

updateArticleByRequest ::
     MonadIO m
  => Text
  -> Text
  -> Maybe Text
  -> Maybe Text
  -> Maybe Text
  -> ReaderT SqlBackend m ()
updateArticleByRequest changer slug mtitle mdesc mbody = do
  now <- liftIO getCurrentTime
  update $ \article -> do
    set
      article
      [ updateByMaybe mtitle article ArticleTitle
      , updateByMaybe mdesc article ArticleDescription
      , updateByMaybe mbody article ArticleBody
      , ArticleUpdatedAt =. just (val now)
      ]
    where_ (article ^. ArticleSlug ==. val slug)
    where_ $
      exists $
      from $ \author -> do
        where_ (author ^. UserUsername ==. val changer)
        where_ (author ^. UserId ==. article ^. ArticleAuthorId)
        where_ (article ^. ArticleSlug ==. val slug)

insertFavorite ::
     ( PersistUniqueWrite backend
     , PersistQueryWrite backend
     , BackendCompatible SqlBackend backend
     , MonadIO m
     )
  => Text
  -> Text
  -> ReaderT backend m ()
insertFavorite username slug = do
  insertSelect $
    from $ \(article, user) -> do
      where_ (user ^. UserUsername ==. val username)
      where_ (article ^. ArticleSlug ==. val slug)
      return $ Favorited <# (user ^. UserId) <&> (article ^. ArticleId)

deleteFavorite ::
     ( PersistUniqueWrite backend
     , PersistQueryWrite backend
     , BackendCompatible SqlBackend backend
     , MonadIO m
     )
  => Text
  -> Text
  -> ReaderT backend m ()
deleteFavorite username slug = do
  delete $
    from $ \favorite -> do
      where_ $
        exists $
        from $ \(follower, article) -> do
          where_ (favorite ^. FavoritedUserId ==. follower ^. UserId)
          where_ (favorite ^. FavoritedArticleId ==. article ^. ArticleId)
          where_ (follower ^. UserUsername ==. val username)
          where_ (article ^. ArticleSlug ==. val slug)
