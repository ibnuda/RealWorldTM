{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE Rank2Types            #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE TypeFamilies          #-}
module Que.Comments where

import           Protolude          hiding (from, get, on, (<&>))

import           Data.Time
import           Database.Esqueleto

import           Model

selectCommentsFromArticles ::
     ( PersistUniqueRead backend
     , PersistQueryRead backend
     , BackendCompatible SqlBackend backend
     , MonadIO m
     )
  => Maybe Text
  -> Text
  -> ReaderT backend m [(Entity Comment, Entity User, Value Bool)]
selectCommentsFromArticles musername slug = do
  select $
    from $ \(article `InnerJoin` comment `InnerJoin` commat) -> do
      let following Nothing = val False
          following (Just username) =
            case_
              [ when_
                  (exists $
                   from $ \(follow, user) -> do
                     where_ (follow ^. FollowAuthorId ==. commat ^. UserId)
                     where_ (follow ^. FollowFollowerId ==. user ^. UserId)
                     where_ (user ^. UserUsername ==. val username))
                  then_
                  (val True)
              ]
              (else_ (val False))
      on (comment ^. CommentUserId ==. commat ^. UserId)
      on (article ^. ArticleId ==. comment ^. CommentArticleId)
      where_ (article ^. ArticleSlug ==. val slug)
      orderBy [asc (comment ^. CommentId)]
      return (comment, commat, following musername)

-- TODO: Fix this, man.
-- I don't like it when a single activity takes more than two queries.
insertComment ::
     ( BaseBackend backend ~ SqlBackend
     , PersistStoreWrite backend
     , MonadIO m
     , PersistUniqueRead backend
     )
  => Text
  -> Text
  -> Text
  -> ReaderT backend m (Maybe (Entity Comment), Maybe (Entity User))
insertComment username slug body = do
  now <- liftIO getCurrentTime
  muser <- getBy (UniqueUsername username)
  mart <- getBy (UniqueSlug slug)
  case (muser, mart) of
    (Just u, Just art) -> do
      comment <-
        insertEntity (Comment body now Nothing (entityKey art) (entityKey u))
      return (Just comment, muser)
    _ -> return (Nothing, Nothing)

deleteComment ::
     ( PersistUniqueWrite backend
     , PersistQueryWrite backend
     , BackendCompatible SqlBackend backend
     , MonadIO m
     )
  => Text
  -> Text
  -> Key Comment
  -> ReaderT backend m ()
deleteComment username slug cid = do
  delete $
    from $ \comment -> do
      where_ $
        exists $
        from $ \(user) -> do
          where_ (user ^. UserUsername ==. val username)
          where_ (user ^. UserId ==. comment ^. CommentUserId)
      where_ $
        exists $
        from $ \article -> do where_ (article ^. ArticleSlug ==. val slug)
      where_ (comment ^. CommentId ==. val cid)
