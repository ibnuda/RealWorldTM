{-# LANGUAGE FlexibleContexts #-}
module Handler.UserProfile where

import           Protolude

import           Database.Esqueleto
import           Servant
import           Servant.Auth.Server

import           Import

getUserProfileHandler ::
     MonadIO m => AuthResult User -> Text -> AppT m ResponseProfile
getUserProfileHandler authres authorname = do
  _ <-
    (runDb (getBy (UniqueUsername authorname))) >>=
    (maybeToHandler identity err404)
  muserf <-
    runDb
      (selectUserProfileByUsername
         (fmap userUsername (authresToMaybe authres))
         authorname)
  case muserf of
    [] -> throwError err404
    ((author, following):_) ->
      return
        (ResponseProfile
           (userToResponseProfile (entityVal author) (unValue following)))

postUserProfileFollowHandler :: MonadIO m => AuthResult User -> Text -> AppT m NoContent
postUserProfileFollowHandler (Authenticated user) username = do
  _ <- errorAlreadyExists =<< (runDb (checkExistenceOfFollowing (userUsername user) username))
  runDb (insertFollowing (userUsername user) username)
  return NoContent
postUserProfileFollowHandler _ _ = throwError err401

deleteUserProfileFollowHandler :: MonadIO m => AuthResult User -> Text -> AppT m NoContent
deleteUserProfileFollowHandler (Authenticated user) username = do
  _ <- errorDoesnotExists =<< (runDb (checkExistenceOfFollowing (userUsername user) username))
  runDb (deleteFollowing (userUsername user) username)
  return NoContent
deleteUserProfileFollowHandler _ _ = throwError err401
