{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeOperators         #-}
module Handler.Articles where

import           Protolude           hiding (get)

import           Database.Esqueleto
import           Servant
import           Servant.Auth.Server

import           Import

getArticlesHandler ::
     (MonadIO m)
  => AuthResult User
  -> Maybe Text
  -> Maybe Text
  -> Maybe Text
  -> Maybe Int64
  -> Maybe Int64
  -> AppT m ResponseMultiArticle
getArticlesHandler authres mtag mauthor mfavorited mlimit moffset = do
  something <-
    runDb
      (selectLatestArticle
         (fmap userUsername (authresToMaybe authres))
         False
         Nothing
         mtag
         mauthor
         mfavorited
         (fromMaybe 20 mlimit)
         (fromMaybe 0 moffset))
  return
    (ResponseMultiArticle
       (map queryArticlesResultToResponse something)
       (length something))

queryArticlesResultToResponse ::
     ( Entity Article
     , Entity User
     , Value (Maybe [Text])
     , Value Int
     , Value Bool
     , Value Bool)
  -> ResponseArticleBody
queryArticlesResultToResponse (entart, entaut, vtags, vfavs, vfaving, vfoll) =
  let art = entityVal entart
      aut = entityVal entaut
      tags = unValue vtags
      favs = unValue vfavs
      faving = unValue vfaving
      foll = unValue vfoll
   in (articleToResponse art tags faving (fromIntegral favs) aut foll)

getArticleSlugHandler ::
     MonadIO m => AuthResult User -> Text -> AppT m ResponseArticle
getArticleSlugHandler authres slug = do
  marticle <-
    runDb
      (selectLatestArticle
         (fmap userUsername (authresToMaybe authres))
         False
         (Just slug)
         Nothing
         Nothing
         Nothing
         1
         0)
  handler marticle
  where
    handler []    = throwError err404
    handler (x:_) = return (ResponseArticle (queryArticlesResultToResponse x))

postArticleHandler ::
     MonadIO m
  => AuthResult User
  -> RequestCreateArticle
  -> AppT m ResponseArticle
postArticleHandler (Authenticated user) (RequestCreateArticle req) = do
  runDb (insertArticle (userUsername user) req)
postArticleHandler _ _ = throwError err401

deleteArticleHandler :: MonadIO m => AuthResult User -> Text -> AppT m NoContent
deleteArticleHandler (Authenticated user) slug = do
  runDb (deleteArticleBySlug (userUsername user) slug)
  return NoContent
deleteArticleHandler _ _ = throwError err401

putArticleHandler :: MonadIO m => AuthResult User -> Text -> RequestUpdateArticle -> AppT m ResponseArticle
putArticleHandler (Authenticated _) _ (RequestUpdateArticle (RequestUpdateArticleBody Nothing Nothing Nothing)) =
  throwError err422
putArticleHandler (Authenticated user) slug (RequestUpdateArticle (RequestUpdateArticleBody mtitle mdesc mbody)) = do
  marticle <-
    runDb $ do
      (updateArticleByRequest (userUsername user) slug mtitle mdesc mbody)
      selectLatestArticle
        (Just (userUsername user))
        False
        (Just slug)
        Nothing
        Nothing
        Nothing
        1
        0
  handler marticle
  where
    handler []    = throwError err404
    handler (x:_) = return (ResponseArticle (queryArticlesResultToResponse x))
putArticleHandler _ _ _ = throwError err401

getCommentsArticleHandler ::
     MonadIO m => AuthResult User -> Text -> AppT m ResponseMultiComment
getCommentsArticleHandler authres slug = do
  _ <- (runDb (getBy (UniqueSlug slug))) >>= (maybeToHandler identity err404)
  comments <-
    runDb
      (selectCommentsFromArticles
         (fmap userUsername (authresToMaybe authres))
         slug)
  return (ResponseMultiComment (map queryCommentResultToResponse comments))

-- UGLY.
postCommentToArticleHandler ::
     MonadIO m => AuthResult User -> Text -> RequestComment -> AppT m ResponseComment
postCommentToArticleHandler (Authenticated user) slug (RequestComment (RequestCommentBody body)) = do
  _ <- (runDb (getBy (UniqueSlug slug))) >>= (maybeToHandler identity err404)
  mcomment <- runDb (insertComment (userUsername user) slug body)
  narcissisticprick <- runDb (isNarcissist (userUsername user))
  case mcomment of
    (Nothing, _) -> throwError (err500 {errBody = "Cannot create comment"})
    (Just c, Just u) ->
      return
        (ResponseComment
           (queryCommentResultToResponse
              (c, u, (Value (not (null narcissisticprick))))))
    _ -> throwError err500
postCommentToArticleHandler _ _ _ = throwError err401

deleteCommentHandler :: MonadIO m => AuthResult User -> Text -> Int64 -> AppT m NoContent
deleteCommentHandler (Authenticated user) slug cid = do
  _ <- (runDb (getBy (UniqueSlug slug))) >>= (maybeToHandler identity err404)
  runDb (deleteComment (userUsername user) slug (toSqlKey cid))
  return NoContent
deleteCommentHandler _ _ _ = throwError err401

queryCommentResultToResponse ::
     (Entity Comment, Entity User, Value Bool) -> ResponseCommentBody
queryCommentResultToResponse (comment, Entity _ user, Value following) =
  let profile = userToResponseProfile user following
  in (commentEntityToResponse comment profile)

commentEntityToResponse ::
  Entity Comment -> ResponseProfileBody -> ResponseCommentBody
commentEntityToResponse (Entity cid comment) profile =
  ResponseCommentBody
    (fromSqlKey cid)
    (commentCreatedAt comment)
    (commentUpdatedAt comment)
    (commentBody comment)
    profile

postFavoriteArticleHandler ::
     MonadIO m
  => AuthResult User
  -> Text
  -> AppT m NoContent
postFavoriteArticleHandler (Authenticated user) slug = do
  _ <- (runDb (getBy (UniqueSlug slug))) >>= (maybeToHandler identity err404)
  _ <- (runDb (checkExistenceOfFavorite (userUsername user) slug)) >>= errorAlreadyExists
  runDb (insertFavorite (userUsername user) slug)
  return NoContent
postFavoriteArticleHandler _ _ = throwError err401

deleteFavoriteArticleHandler ::
     MonadIO m => AuthResult User -> Text -> AppT m NoContent
deleteFavoriteArticleHandler (Authenticated user) slug = do
  _ <- (runDb (getBy (UniqueSlug slug))) >>= (maybeToHandler identity err404)
  _ <- (runDb (checkExistenceOfFavorite (userUsername user) slug)) >>= errorDoesnotExists
  runDb (deleteFavorite (userUsername user) slug)
  return NoContent
deleteFavoriteArticleHandler _ _ = throwError err401

getFeedHandler ::
     MonadIO m
  => AuthResult User
  -> Maybe Int64
  -> Maybe Int64
  -> AppT m ResponseMultiArticle
getFeedHandler (Authenticated user) mlimit moffset = do
  something <-
    runDb
      (selectLatestArticle
         (Just (userUsername user))
         True
         Nothing
         Nothing
         Nothing
         Nothing
         (fromMaybe 20 mlimit)
         (fromMaybe 0 moffset))
  return
    (ResponseMultiArticle
       (map queryArticlesResultToResponse something)
       (length something))
getFeedHandler _ _ _ = throwError err401
