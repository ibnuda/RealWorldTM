{-# LANGUAGE RecordWildCards #-}
module Handler.UserInformation where

import           Protolude

import           Crypto.BCrypt
import           Database.Esqueleto
import           Servant
import           Servant.Auth.Server

import           Import

getUserInformationHandler :: MonadIO m => AuthResult User -> AppT m ResponseUser
getUserInformationHandler (Authenticated user) = do
  token <- generateToken user
  return (ResponseUser (userToResponseUser user (Just token)))
getUserInformationHandler _ = throwError err401

putUserInformationHandler ::
     MonadIO m => AuthResult User -> RequestUpdateUser -> AppT m ResponseUser
putUserInformationHandler (Authenticated user) (RequestUpdateUser RequestUpdateUserBody {..}) = do
  hashedpass <- liftIO (mapM generatePassword requpdtuserbodyPassword)
  runDb
    (updateUser
       (userUsername user)
       requpdtuserbodyEmail
       requpdtuserbodyUsername
       hashedpass
       requpdtuserbodyBio
       requpdtuserbodyImage)
  case requpdtuserbodyUsername of
    Nothing -> do
      (Just (Entity _ u)) <- runDb (getUserByUsername (userUsername user))
      token <- generateToken u
      return (ResponseUser (userToResponseUser u (Just token)))
    Just x -> do
      (Just (Entity _ u)) <- runDb (getUserByUsername x)
      token <- generateToken u
      return (ResponseUser (userToResponseUser u (Just token)))
putUserInformationHandler _ _ = throwError err401

postRegistrationHandler ::
     MonadIO m => RequestRegistration -> AppT m ResponseUser
postRegistrationHandler (RequestRegistration reqreg) = do
  hashedpass <- liftIO (generatePassword (reqregbodyPassword reqreg))
  let reqreg' = reqreg {reqregbodyPassword = hashedpass}
  muser <- runDb (insertUserFromRegistration reqreg')
  user <- maybeToHandler identity err401 muser
  token <- generateToken user
  return (ResponseUser (userToResponseUser user (Just token)))

postLoginHandler ::
     MonadIO m => RequestLogin -> AppT m ResponseUser
postLoginHandler (RequestLogin (RequestLoginBody email password)) = do
  muser <- runDb (selectUserByEmail email)
  (Entity _ user) <- maybeToHandler identity err401 muser
  _ <-
    trueToHandler
      (validatePassword (textToBChar (userPassword user)) (textToBChar password))
  token <- generateToken user
  return (ResponseUser (userToResponseUser user (Just token)))
