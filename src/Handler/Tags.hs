{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeOperators         #-}
module Handler.Tags where

import           Protolude

import           Database.Esqueleto

import           Import

getTagsHandler :: MonadIO m => AppT m ResponseTags
getTagsHandler = do
  tags <- runDb selectTags
  return (resptags tags)
  where
    resptags xs = ResponseTags (map (\(Entity _ t) -> tagName t) xs)
